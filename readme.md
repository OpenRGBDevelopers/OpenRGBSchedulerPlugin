# Scheduler Plugin 

[![pipeline status](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/badges/master/pipeline.svg)](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/commits/master)

## What is this?

This is a plugin for [OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB) that allows you to schedule some actions (load profile, turn off)

## Experimental (Master)

* [Windows 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Windows%2032)
* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Windows%2064)
* [Linux i386](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Linux%20i386)
* [Linux amd64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Linux%20amd64)
* [Linux arm64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Linux%20arm64)
* [Linux armhf](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Linux%20armhf)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=MacOS%20ARM64)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=MacOS%20Intel)

## Stable (0.9)

* [Windows 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/8321400130/artifacts/download)
* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/8321400179/artifacts/download)
* [Linux 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/8321399782/artifacts/download)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/8321399856/artifacts/download)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/8321399969/artifacts/download)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/8321400066/artifacts/download)

## Stable (0.8)

* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/3418216906/artifacts/download)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/3418216904/artifacts/download)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/3418216907/artifacts/download)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/3418216908/artifacts/download)

## How do I install it?

* Download and extract the correct files depending on your system
* Launch OpenRGB
* From the Settings -> Plugins menu, click the "Install plugin" button
