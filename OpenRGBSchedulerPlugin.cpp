#include "OpenRGBSchedulerPlugin.h"
#include "SchedulerMainView.h"
#include <QHBoxLayout>

ResourceManagerInterface* OpenRGBSchedulerPlugin::RMPointer = nullptr;

OpenRGBPluginInfo OpenRGBSchedulerPlugin::GetPluginInfo()
{
    printf("[OpenRGBSchedulerPlugin] Loading plugin info.\n");

    OpenRGBPluginInfo info;

    info.Name           = "Scheduler plugin";
    info.Description    = "Schedule some OpenRGB actions";
    info.Version        = VERSION_STRING;
    info.Commit         = GIT_COMMIT_ID;
    info.URL            = "https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin";

    info.Icon.load(":/OpenRGBSchedulerPlugin.png");

    info.Location       =  OPENRGB_PLUGIN_LOCATION_TOP;
    info.Label          =  "Scheduler";
    info.TabIconString  =  "Scheduler";

    info.TabIcon.load(":/OpenRGBSchedulerPlugin.png");

    return info;
}

unsigned int OpenRGBSchedulerPlugin::GetPluginAPIVersion()
{
    printf("[OpenRGBSchedulerPlugin] Loading plugin API version.\n");

    return OPENRGB_PLUGIN_API_VERSION;
}

void OpenRGBSchedulerPlugin::Load(ResourceManagerInterface* resource_manager_ptr)
{
    printf("[OpenRGBSchedulerPlugin] Loading plugin.\n");

    RMPointer = resource_manager_ptr;
}

QWidget* OpenRGBSchedulerPlugin::GetWidget()
{
    printf("[OpenRGBSchedulerPlugin] Creating widget.\n");

    return new SchedulerMainView();
}

QMenu* OpenRGBSchedulerPlugin::GetTrayMenu()
{
    printf("[OpenRGBSchedulerPlugin] Creating tray menu.\n");

    return nullptr;
}

void OpenRGBSchedulerPlugin::Unload()
{
    printf("[OpenRGBSchedulerPlugin] Time to call some cleaning stuff.\n");
}

OpenRGBSchedulerPlugin::~OpenRGBSchedulerPlugin()
{
    printf("[OpenRGBSchedulerPlugin] Time to free some memory.\n");
}
